FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/monev-api-0.0.1-SNAPSHOT.jar monev-api.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/monev-api.jar"]
