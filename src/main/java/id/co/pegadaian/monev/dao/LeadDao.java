package id.co.pegadaian.monev.dao;

import org.springframework.data.repository.CrudRepository;

import id.co.pegadaian.monev.entity.Lead;

public interface LeadDao extends CrudRepository<Lead, String>{

}
