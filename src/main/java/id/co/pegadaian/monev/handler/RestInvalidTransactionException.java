/**
 * https://github.com/stormpath/spring-mvc-rest-exhandler
 */
package id.co.pegadaian.monev.handler;

public class RestInvalidTransactionException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RestInvalidTransactionException(String msg) {
        super(msg);
    }
}
