package id.co.pegadaian.monev.services;

import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.pegadaian.monev.dao.LeadDao;
import id.co.pegadaian.monev.dto.LeadAddRequest;
import id.co.pegadaian.monev.dto.LeadAddResponse;
import id.co.pegadaian.monev.entity.Lead;
import id.co.pegadaian.monev.handler.RestInvalidTransactionException;

@Service
@Transactional
public class LeadServices {
	private static final Logger logger = LoggerFactory.getLogger(LeadServices.class);

	@Autowired
	LeadDao leadDao;
	
	public LeadAddResponse saveLead(LeadAddRequest req) throws Exception {
		logger.info("Start : Services New Lead Handphone no : " + req.getNoHp());
		LeadAddResponse response = null;
		try {
			Optional<Lead> leadOptional = leadDao.findById(req.getNoHp().toString());
        	if(leadOptional.isPresent()){
        		logger.info("Lead Found for hp : " + req.getNoHp());
				throw new RestInvalidTransactionException("Lead " + req.getNoHp() + " Duplicate");
            }else {
            	Lead lead = new Lead();
            	lead.setCreateBy("pevita");
                lead.setCreateDate(new Date());
                lead.setClientId(req.getClientId());
            	lead.setJenisKelamin(req.getJenisKelamin());
            	lead.setKategoriAds(req.getKategoriAds());
            	lead.setNama(req.getNama());
            	lead.setNoHp(req.getNoHp());
            	lead.setUsia(req.getUsia());
            	
                leadDao.save(lead);
                
                response = new LeadAddResponse();
                response.setNoHp(req.getNoHp());
                
            }
		} catch (Exception e) {
			logger.error("Exception in saveLead - " + e.getMessage());
			throw e;
		}
		
		return response;
	}	
}