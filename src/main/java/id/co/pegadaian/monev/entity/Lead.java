package id.co.pegadaian.monev.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Table(name = "voucher_lead")
@Data
public class Lead implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "no_hp", nullable = false, length = 15)
    private String noHp;
	
	@Column(name = "nama", length = 100)
    private String nama;
    
	@Column(name = "kategori_ads", length = 100)
    private String kategoriAds; 
    
	@Column(name = "usia", length = 60)
    private String usia;
    
	@Column(name = "jenis_kelamin", length = 15)
    private String jenisKelamin;
    
	@Column(name = "create_by", length = 10)
    private String createBy;
    
	@Column(name = "client_id", length = 10)
    private String clientId;
    
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    
}
