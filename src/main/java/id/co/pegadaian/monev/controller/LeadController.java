package id.co.pegadaian.monev.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import id.co.pegadaian.monev.dto.LeadAddRequest;
import id.co.pegadaian.monev.dto.LeadAddResponse;
import id.co.pegadaian.monev.dto.ResponseService;
import id.co.pegadaian.monev.handler.RestInvalidTransactionException;
import id.co.pegadaian.monev.services.LeadServices;
import id.co.pegadaian.monev.util.M24RestConstant.RESPONSE;

@RestController
@RequestMapping(value="/lead")
public class LeadController {

	private static final Logger logger = LoggerFactory.getLogger(LeadController.class);
	
	@Autowired LeadServices serviceLead;

	@RequestMapping(value="/add",  method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseService saveLead(@RequestBody LeadAddRequest req) {
		logger.info("Incoming POST lead/add/" + req.getNoHp());
		ResponseService responseService = new ResponseService();
		try {
			LeadAddResponse addResponse = serviceLead.saveLead(req);
			if (addResponse != null) {
				responseService.setResponseCode(RESPONSE.APPROVED.getCode());
				responseService.setResponseDesc(RESPONSE.APPROVED.getDescription());
				Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
				responseService.setData(gson.toJson(addResponse));
			} else {
				responseService.setResponseCode(RESPONSE.ACCOUNT_NOT_FOUND.getCode());
				responseService.setResponseDesc(RESPONSE.ACCOUNT_NOT_FOUND.getDescription());
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			if (e instanceof RestInvalidTransactionException) {
				responseService.setResponseCode(RESPONSE.INVALID_TRANSACTION.getCode());
				responseService.setResponseDesc(RESPONSE.INVALID_TRANSACTION.getDescription() + " " + e.getMessage());
			} else {
				responseService.setResponseCode(RESPONSE.HTTP_INTERNAL_ERROR.getCode());
				responseService.setResponseDesc("System Gagal Mengambil Data");
			}
		}
		
		logger.info("OutGoing POST lead/add/" + req.getNoHp());
		return responseService;
	}
}
