package id.co.pegadaian.monev.dto;

import lombok.Data;

@Data
public class LeadAddResponse {
    
    private String noHp;
    
}
