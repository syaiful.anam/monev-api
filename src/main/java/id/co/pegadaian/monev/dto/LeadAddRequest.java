package id.co.pegadaian.monev.dto;

import lombok.Data;

@Data
public class LeadAddRequest {
	
	private String clientId;
	
	private String noHp;
	
	private String nama;
	
	private String jenisKelamin;
	
	private String kategoriAds;
	
	private String usia;
	
}
