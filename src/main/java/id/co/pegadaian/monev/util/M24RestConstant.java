package id.co.pegadaian.monev.util;

public class M24RestConstant {
	public static enum RESPONSE{
		APPROVED("00","Approved"),
		HTTP_NOT_FOUND("X4","There is No Resource Path"),
		HTTP_INTERNAL_ERROR("X5","Service Internal Error"),
		INVALID_TRANSACTION("12","Invalid Transaction"),
		INVALID_AMOUNT("13","Jumlah Tidak Sesuai"),
		ACCOUNT_NOT_FOUND("14","Rekening/Data Tidak ditemukan"),
		INVALID_ACCOUNT("15","Rekening Blokir / Tidak Aktif"),
		WRONG_FORMAT_DATA("30","Format Data Salah"),
		
		/**
		 * Hanya Unuk RestSwithcing
		 */
		CA_NOT_REGISTERED_OR_INVALID_CODE("31","CA Not Registered/Wrong Password"),
		TRANSACTION_TIMEOUT("68","Transaction Timeout"),
		TAGIHAN_SUDAH_TERBAYAR("88","Tagihan Sudah Terbayar"),
		CUT_OFF_TOME("90","Cut Off Time"),
		SYSTEM_MAINTENANCE("96","System Maintenance"),
		GENERAL_ERROR("98","General Error"),
		
		;
		private String code,description;
		RESPONSE(String code,String description){
			this.code=code;
			this.description=description;
		}
		public String getCode(){
			return this.code;
		}
		public String getDescription(){
			return this.description;
		}
	}
}
